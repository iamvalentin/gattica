require 'bundler/setup'
require 'json'
require 'gattica'

email = ENV['GOOGLE_EMAIL']
password = ENV['GOOGLE_PASSWORD']
ga = Gattica.new(email: email, password: password) 
ga.profile_id = 70158216

rows = []
has_more_rows = true
start_index = 0

while(has_more_rows) do
  data = ga.get({
    start_date: '2014-05-28',
    end_date: '2014-05-28',
    dimensions: %w(landingPagePath),
    metrics: %w(sessions),
    max_results: 10_000,
    start_index: start_index
  })
  start_index += 10_000
  data = data.to_h['points']
  has_more_rows = data.size > 0
  rows = rows + data.map(&:dimensions)
  puts "Got #{rows.size} rows"
end

File.open('/Users/valentin/landing_pages.json', 'w') do |f|
  f << JSON.generate(rows)
end






